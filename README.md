# README #

Esse documento eh um guia basico de funcionamento deste software de modelagem de 1/4 de carro.

### Informacoes contidas ###

- Ambientacao necessaria
- Como utilizar?
- Contibuicoes
- Contato

### Ambientacao necessaria ###

Para o desenvolvimento desse software foram necessarias algumas configuracoes e instalacoes de bibliotecas auxiliares, por isso deve-se instalar em sua maquina:

- Python versao superior a 2.6
- PySimpleGUI
- Scipy
- MathPlotLib
- Numpy

#### Guia rapido de instacao ####

Com o Python ja instalado corretamente em sua maquina, voce deve, para cada biblioteca:

- Se Python2 pip install **nome da biblioteca**
- Se Python3 pip3 install **nome da biblioteca**


### Como utilizar? ###

### Contribuicoes ###

### Contato ###

- email: j199415@dac.unicamp.br
