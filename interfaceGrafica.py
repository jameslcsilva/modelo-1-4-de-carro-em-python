import PySimpleGUI as sg
import numpy as np
from math import *

sg.theme('DarkAmber')   # Add a touch of color
# All the stuff inside your window.

# Caracteristicas de Layout Programa
fonteTitulo = ('Arial', 24)
fonteCorpo = ('Arial', 18)
sizeTexto = (3, 1)
sizeLacuna = (5, 1)
sizeColuna = (30, 1)


Resposta = 'Resposta_Deformacao.png'
# Create the Window
def mostrarTela():
    #Colunas entradas de parametro
    coluna1 = [
        [sg.Text('Dados Pista', font=fonteTitulo)],
        [sg.Text('Y: ', font=fonteCorpo, size=sizeTexto), sg.Input(size=sizeLacuna, font=fonteCorpo)],
        [sg.Text('v: ', font=fonteCorpo, size=sizeTexto), sg.Input(size=sizeLacuna, font=fonteCorpo)],
        [sg.Text('w: ', font=fonteCorpo, size=sizeTexto), sg.Input(size=sizeLacuna, font=fonteCorpo)],
        [sg.Text('n: ', font=fonteCorpo, size=sizeTexto), sg.Input(size=sizeLacuna, font=fonteCorpo)],
        [sg.Text('Modelo Pista', font=fonteTitulo)],
        [sg.Image('pista.png')]
    ]
    coluna2 = [
        [sg.Text('Dados Pneu', font=fonteTitulo)],
        [sg.Text('mu: ', font=fonteCorpo, size=sizeTexto), sg.Input(size=sizeLacuna, font=fonteCorpo)],
        [sg.Text('cu: ', font=fonteCorpo, size=sizeTexto), sg.Input(size=sizeLacuna, font=fonteCorpo)],
        [sg.Text('ku: ', font=fonteCorpo, size=sizeTexto), sg.Input(size=sizeLacuna, font=fonteCorpo)],
        [sg.Text('Dados Chassi', font=fonteTitulo)],
        [sg.Text('ms: ', font=fonteCorpo, size=sizeTexto), sg.Input(size=sizeLacuna, font=fonteCorpo)],            
        [sg.Text('cs: ', font=fonteCorpo, size=sizeTexto), sg.Input(size=sizeLacuna, font=fonteCorpo)],
        [sg.Text('ks: ', font=fonteCorpo, size=sizeTexto), sg.Input(size=sizeLacuna, font=fonteCorpo)]
    ]
    coluna3 = [
        [sg.Text('Modelo carro', font=fonteTitulo)],
        [sg.Image('modelo14.png')]
    ]
    tab0Layout = [
        [sg.Text('Observacoes de utilizacao do software', font=fonteTitulo)],
        [sg.Text('* Utilize as seguintes unidades:', font=fonteCorpo)],
        [sg.Text('  * m = [kg], c = [N.s/m], k = [N/m]', font=fonteCorpo)],
        [sg.Text('  * t = [s], y = [mm], v = [m/s]', font=fonteCorpo)],
        [sg.Text('* n = numero de pontos da simulacao\n', font=fonteCorpo)],
        [sg.HSeparator()],
        [sg.Text('Parametros Atuais', font=fonteTitulo)],
        [sg.Text('ms = ', font=fonteCorpo, size=sizeTexto), sg.InputText('values', size=sizeLacuna, font=fonteCorpo), sg.Text('cs = ', font=fonteCorpo, size=sizeTexto), sg.InputText('values', size=sizeLacuna, font=fonteCorpo), sg.Text('ks = ', font=fonteCorpo, size=sizeTexto), sg.InputText('values', size=sizeLacuna, font=fonteCorpo), sg.Text('mu = ', font=fonteCorpo, size=sizeTexto), sg.InputText('values', size=sizeLacuna, font=fonteCorpo), sg.Text('cu = ', font=fonteCorpo, size=sizeTexto), sg.InputText('values', size=sizeLacuna, font=fonteCorpo)],
        [sg.Text('Y = ', font=fonteCorpo, size=sizeTexto), sg.InputText('values', size=sizeLacuna, font=fonteCorpo), sg.Text('v = ', font=fonteCorpo, size=sizeTexto), sg.InputText('values', size=sizeLacuna, font=fonteCorpo), sg.Text('t = ', font=fonteCorpo, size=sizeTexto), sg.InputText('values', size=sizeLacuna, font=fonteCorpo), sg.Text('n = ', font=fonteCorpo, size=sizeTexto), sg.InputText('values', size=sizeLacuna, font=fonteCorpo), sg.Text('ku = ', font=fonteCorpo, size=sizeTexto), sg.InputText('values', size=sizeLacuna, font=fonteCorpo)],
    [sg.Text('*** Os dados aqui apresentados referem-se a ultima\nsimulacao realizada ***\n', font=fonteCorpo)],
        [sg.HSeparator()],
        [sg.Text('Parametros de referencia', font=fonteTitulo)]

    ]

    tab1Layout = [
        [sg.Column(coluna1, element_justification = 'c'), sg.VSeperator(),
        sg.Column(coluna2, element_justification = 'c'), sg.VSeperator(),
        sg.Column(coluna3, element_justification = 'c')],
        [sg.HSeparator()],
        [sg.Button('Setar Valores'), sg.Button('Valores Padrao')],
        [sg.HSeparator()],
        [sg.Text('Modelo de pista de entrada', font=fonteTitulo)],
        [sg.Image('Modelo_da_Pista.png')]

    ]

    tab2Layout = [
        [sg.Text('Resposta da deformacoes da pista, chassi e pneu', font=fonteTitulo)],
        [sg.Image('Resposta_Deformacao.png')],
        [sg.Image('Resposta_Chassi_Modulo.png')],
        [sg.Image('Resposta_Chassi_Fase.png')]
    ]

    tab3Layout = [
        
    ]

    # Layout Tela Inicial com entrada de parametros do veiculo
    layout = [
        [sg.TabGroup([[sg.Tab('Intrucoes de utilizacao', tab0Layout)],[sg.Tab('Dados do sistema', tab1Layout)], [sg.Tab('Resultado da simulacao', tab2Layout)], [sg.Tab('Animacao', tab3Layout)]])]
    ]

    window = sg.Window('Ambiente de Simulacao', layout)
    # Event Loop to process "events" and get the "values" of the inputs
    event, values = window.read()

    # Valores Padrao para um veiculo
    if event == 'Valores Padrao':
        values = {'0':  2e-3, '1':  100, '2':  2, '3':  100, '4':  375, '5': 50 , '6':  500, '7':  500, '8':  30e3, '9':  300e3}
    
    if event == sg.WIN_CLOSED:
        exit()

    
    window.close()
    #Converte meu dicionario para uma lista
    listValues = list(values.values())

    #Retorna minha lista de parametros
    return np.array(listValues, dtype = float)
