import matplotlib.pyplot as plt

def gerarGrafico1(t, dPista, dPneu, dChassi, grandeza, unidade, titulo):
    tituloPng = titulo + '.png'
    fig, ax = plt.subplots()
    ax.plot(t, dPista, label='pista')
    ax.plot(t, dPneu, label='pneu')
    ax.plot(t, dChassi, label='chassi')
    ax.legend(loc='upper right')
    ax.set_title(titulo)
    ax.grid()
    fig.set_figwidth(7)
    fig.set_figheight(2)
    fig.savefig(tituloPng)
    return 1


def gerarGrafico(t, x, item, titulo, grandeza):
    tituloPng = titulo + '.png'
    fig, ax = plt.subplots()
    ax.plot(t, x, label=item)
    ax.legend(loc='upper right')
    ax.set_title(titulo)
    ax.grid()
    fig.set_figwidth(7)
    fig.set_figheight(2)
    fig.savefig(tituloPng)
    return 1