from gerarGrafico import *
from interfaceGrafica import *
import numpy as np
from sistemaModelo1_4 import *
import matplotlib.pyplot as plt


while(1):
    parametros = mostrarTela()
    y, t = pista(parametros)
    gerarGrafico(t, y, 'pista', 'Modelo_da_Pista', 'Deslocamento')
    M, C, K, F = constroiMatrizes(parametros)
    print(M)
    print()
    print(C)
    print()
    print(K)
    print()
    print(F)
    print()
    Z = matrizImpedancia(M, C, K, parametros[1])
    print(Z)
    print()
    xPA = respostaPermanenteAmplitude(Z, F)
    print(xPA)
    print()
    xPT, t = respostaPermanenteTemporal(xPA, parametros[2], parametros[3], parametros[1])
    print(xPT)
    print()
    print()
    xPTM = getModulo(xPT)
    print(xPTM)
    print()
    xPTF = getFase(xPT)
    print(xPTF)
    print()

    xSM, xUM = separaResposta(xPTM)
    xSF, xUF = separaResposta(xPTF)

    print(xSM)
    print(xUM)
    print(xSF)
    print(xUF)

    gerarGrafico(t, xSM, 'chassi', 'Resposta_Chassi_Modulo_vs_t', 'Módulo')
    gerarGrafico(t, xSF, 'chassi', 'Resposta_Chassi_Fase_vs_t', 'Fase')

    t = tempoPontos(parametros)
    Xs, Xu = modelo1_4(parametros, t)
    gerarGrafico1(t, y, Xu, Xs, 'deformacoes', 'm', 'Resposta_Deformacao')