from scipy.integrate import solve_ivp
import numpy as np
from math import *
import cmath as cm

def tempoPontos(parametros):
    t = parametros[2]
    n = int(parametros[3])
    return np.linspace(0, t, n)

def modelo1_4(parametros, t):
    Y = parametros[0]
    w = parametros[1]
    tf = parametros[2]
    ms = parametros[4]
    mu = parametros[5]
    cs = parametros[6]
    cu = parametros[7]
    ks = parametros[8]
    ku = parametros[9]


    def sistema(t, resposta):
        xs, dxs, xu, dxu = resposta

        d2xs = - (ks*(xs - xu) + cs*(dxs - dxu))/ms
        d2xu = (ks*(xs - xu) + cs*(dxs - dxu) - (ku*(xu - Y*sin(w*t)) + cu*(dxu - Y*cos(w*t))))/mu

        return np.array([dxs, d2xs, dxu, d2xu])

    #Soluciona Sistema Por RK 45
    sol = solve_ivp(sistema, (0, tf), [0, 0, 0, 0], t_eval = t)

    # Obtendo resposta do sistema em regime permanente
    xs = sol.y[0]
    dxs = sol.y[1]

    xu = sol.y[2]
    dxu = sol.y[3]

    # Calculando a partir dos valores encontrados
    msd2xs = - ks*(xs - xu) + cs*(dxs - dxu)
    d2xs =  msd2xs/ms

    mud2xu = ks*(xs - xu) + cs*(dxs - dxu) - (ku*(xu - Y*np.sin(w*t)) + cu*(dxu - Y*np.cos(w*t)))
    d2xu = mud2xu/mu


    return xs, xu



def pista(parametros):
    Y = parametros[0]
    w = parametros[1]
    n = int(parametros[3])
    t = np.linspace(0, parametros[2], n)


    mTotal = parametros[4] + parametros[5]
    y = Y*np.sin(w*t)
    return y, t

def constroiMatrizes(parametros):
    y = parametros[0]
    w = parametros[1]
    ms = parametros[4]
    mu = parametros[5]
    cs = parametros[6]
    cu = parametros[7]
    ks = parametros[8]
    ku = parametros[9]

    M = np.array([[ms, 0],
                  [0, mu]])

    C = np.array([[cs, -cs],
                  [-cs, cs + cu]])

    K = np.array([[ks, -ks],
                  [-ks, ks + ku]])

    F = np.array([[0],
                  [complex(ku*y, cu*w*y)]])

    return M, C, K, F

def matrizImpedancia(M, C, K, w):
    z11 = K[0][0] - M[0][0]*w**2 + C[0][0]*w*1j
    z12 = K[0][1] - M[0][1]*w**2 + C[0][1]*w*1j
    z21 = K[1][0] - M[1][0]*w**2 + C[1][0]*w*1j
    z22 = K[1][1] - M[1][1]*w**2 + C[1][1]*w*1j

    Z = np.array([[z11, z12],
                  [z21, z22]])


    return Z

def respostaPermanenteAmplitude(Z, F):
    iZ = np.linalg.inv(Z)
    print(iZ)
    print()
    xP = iZ@F

    return xP

def respostaPermanenteTemporal(xP, t, n, w):
    n = int(n)
    t = np.linspace(0, t, n)
    xPT = xP*(np.cos(w*t) + np.sin(w*t)*1j)
    

    return xPT, t

def getModulo(xPT):

    xPTM = np.sqrt(xPT.real**2 + xPT.imag**2)

    return xPTM


def getFase(xPT):

    xPTF = np.arctan(xPT.real/xPT.imag)

    return xPTF


def separaResposta(xP):
    return xP[0], xP[1]